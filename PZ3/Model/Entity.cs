﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PZ3.Model
{
    public class Entity
    {
        private string id;
        private string name;
        private double longitude;
        private double latitude;

        public Entity()
        {
        }

        public string Name { get => name; set => name = value; }
        public string Id { get => id; set => id = value; }
        [XmlIgnore]
        public double Longitude { get => longitude; set => longitude = value; }
        [XmlIgnore]
        public double Latitude { get => latitude; set => latitude = value; }

        [XmlIgnore]
        public int RowIndex;

        [XmlIgnore]
        public int ColomnIndex;



        [XmlIgnore]
        public double GridLatitude;

        [XmlIgnore]
        public double GridLongitude;

        [XmlIgnore]
        public bool Connected = false;

    }
}
