﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ3.Model
{
    public class State
    {
        public static int[,] PassedLocation;

        public State Parent;

        public LocationOnGrid Node;

        public State(State parent, LocationOnGrid node)
        {
            if (PassedLocation == null)
            {
                PassedLocation = new int[MainWindow.NumberOfRow + 1, MainWindow.NumberOfColoms + 1];
                for (int i = 0; i <= MainWindow.NumberOfRow; i++)
                {
                    for (int j = 0; j <= MainWindow.NumberOfColoms; j++)
                    {
                        PassedLocation[i, j] = 0;
                    }
                }
            }

            Parent = parent;
            Node = node;
        }

        public List<State> Children()
        {
            List<State> retVal = new List<State>();

            // up
            if (Node.RowIndex != 0
                && !(PassedLocation[Node.RowIndex - 1, Node.ColomnIndex] == 1))
            {
                retVal.Add(new State(this, new LocationOnGrid(Node.RowIndex - 1, Node.ColomnIndex)));
            }
            // down
            if (Node.RowIndex != MainWindow.NumberOfRow
                && !(PassedLocation[Node.RowIndex + 1, Node.ColomnIndex] == 1))
            {
                retVal.Add(new State(this, new LocationOnGrid(Node.RowIndex + 1, Node.ColomnIndex)));
            }
            //left
            if (Node.ColomnIndex != 0
                && !(PassedLocation[Node.RowIndex, Node.ColomnIndex - 1] == 1))
            {
                retVal.Add(new State(this, new LocationOnGrid(Node.RowIndex, Node.ColomnIndex - 1)));
            }
            //right
            if (Node.ColomnIndex != MainWindow.NumberOfColoms
                && !(PassedLocation[Node.RowIndex, Node.ColomnIndex + 1] == 1))
            {
                retVal.Add(new State(this, new LocationOnGrid(Node.RowIndex, Node.ColomnIndex + 1)));
            }

            return retVal;
        }

        public List<LocationOnGrid> Path()        // metoda za vracanje putanje  (uzeto sa predmeta ORI)
        {
            List<LocationOnGrid> path = new List<LocationOnGrid>();
            State tt = this;
            while (tt != null)
            {
                if (MainWindow.MainGrid[tt.Node.RowIndex, tt.Node.ColomnIndex] == 0)
                {
                    MainWindow.MainGrid[tt.Node.RowIndex, tt.Node.ColomnIndex] = 2;
                }
                path.Insert(0, tt.Node);
                tt = tt.Parent;
            }
            return path;
        }
    }
}