﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ3.Model
{
    public class Route
    {
        public string Id;

        public Entity FirstEnd;
        public Entity SecondEnd;

        public List<LocationOnGrid> RouteOnGrid;

        public Route()
        {
            RouteOnGrid = new List<LocationOnGrid>();
        }
    }
}
