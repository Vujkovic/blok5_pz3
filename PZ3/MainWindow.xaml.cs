﻿using PZ3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace PZ3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private NetworkModel NetworkModel;

        public static int NumberOfRow = 199;
        public static int NumberOfColoms = 199;

        public static double xmin = 0.0;
        public static double ymin = 0.0;
        public static double xmax = 0.0;
        public static double ymax = 0.0;

        public int counterneki = 0;

        public static List<double> allLatitudes = new List<double>();
        public static List<double> allLongitudes = new List<double>();

        public static int[,] MainGrid;  //matrica grida 
        public double LongStep; // korak po longitudi
        public double LatitStep; // korak po latitudi
        public Dictionary<string, Route> Routes;
        List<Tuple<string, Entity>> dic = new List<Tuple<string, Entity>>();

        public MainWindow()
        {
            InitializeComponent();
            NetworkModel = new NetworkModel();
            Routes = new Dictionary<string, Route>();
            try
            {
                XmlSerializer serializer = new XmlSerializer(NetworkModel.GetType());
                using (FileStream reader = new FileStream("Geographic.xml", FileMode.Open))
                {
                    NetworkModel = (NetworkModel)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("Error reading Geographic.xml");
                this.Close();
            }

            GetLatLon();
            MainGrid = new int[NumberOfRow + 1, NumberOfColoms + 1];

            LongStep = (ymax - ymin) / NumberOfColoms;
            LatitStep = (xmax - xmin) / NumberOfRow;

            for (int i = 0; i <= NumberOfRow; i++)
            {
                for (int j = 0; j <= NumberOfColoms; j++)
                {
                    MainGrid[i, j] = 0; //incijalizujemo sve na 0 (prazan grid bez nodova)
                }
            }
            Tackanje();

            Putanje();
        }


        public void Tackanje()
        {
            int brojac = 0;
            foreach (SubstationEntity subEntity in NetworkModel.Substations)
            {
                brojac++;
                EntityDrawing(subEntity);

            }
            foreach (NodeEntity ndEntity in NetworkModel.Nodes)
            {
                brojac++;
                EntityDrawing(ndEntity);
            }
            foreach (SwitchEntity swEntity in NetworkModel.Switches)
            {
                brojac++;
                EntityDrawing(swEntity);
            }
            int broj = ShapeCanvas.Children.Count;
        }

        private LocationOnGrid BFSCvorak(LocationOnGrid kandidat)
        {
            // LocationOnGrid trenutni = kandidat;
            Queue<LocationOnGrid> queue = new Queue<LocationOnGrid>();
            queue.Enqueue(kandidat);
            int[,] visited = new int[NumberOfRow + 1, NumberOfColoms + 1];
            visited[kandidat.RowIndex, kandidat.ColomnIndex] = 1;
            while (queue.Count >= 0)
            {
                LocationOnGrid trenutni = queue.Dequeue();
                if (MainGrid[trenutni.RowIndex, trenutni.ColomnIndex] == 0)
                {
                    return trenutni;
                }

                //moving up
                if (trenutni.RowIndex - 1 >= 0 && visited[trenutni.RowIndex - 1, trenutni.ColomnIndex] == 0)
                {

                    queue.Enqueue(new LocationOnGrid(trenutni.RowIndex - 1, trenutni.ColomnIndex));
                    visited[trenutni.RowIndex - 1, trenutni.ColomnIndex] = 1;
                }

                //moving down
                if (trenutni.RowIndex + 1 <= NumberOfRow && visited[trenutni.RowIndex + 1, trenutni.ColomnIndex] == 0)
                {

                    queue.Enqueue(new LocationOnGrid(trenutni.RowIndex + 1, trenutni.ColomnIndex));
                    visited[trenutni.RowIndex + 1, trenutni.ColomnIndex] = 1;
                }

                //moving left
                if (trenutni.ColomnIndex - 1 >= 0 && visited[trenutni.RowIndex, trenutni.ColomnIndex - 1] == 0)
                {

                    queue.Enqueue(new LocationOnGrid(trenutni.RowIndex, trenutni.ColomnIndex - 1));
                    visited[trenutni.RowIndex, trenutni.ColomnIndex - 1] = 1;
                }

                //moving right
                if (trenutni.ColomnIndex + 1 <= NumberOfColoms && visited[trenutni.RowIndex, trenutni.ColomnIndex + 1] == 0)
                {

                    queue.Enqueue(new LocationOnGrid(trenutni.RowIndex, trenutni.ColomnIndex + 1));
                    visited[trenutni.RowIndex, trenutni.ColomnIndex + 1] = 1;
                }
            }

            return null;
        }

        private void EntityDrawing(Entity entity)
        {
            entity.RowIndex = 0;
            entity.ColomnIndex = 0;
            //entity.GridLatitude = double.MinValue;
            // entity.GridLongitude = double.MinValue;

            ReturnGridEntity(entity.Latitude, entity.Longitude, out entity.RowIndex, out entity.ColomnIndex, out entity.GridLatitude, out entity.GridLongitude);

            LocationOnGrid ret = BFSCvorak(new LocationOnGrid(entity.RowIndex, entity.ColomnIndex));
            if (ret != null)
            {
                counterneki++;
                entity.RowIndex = ret.RowIndex;
                entity.ColomnIndex = ret.ColomnIndex;
                MainGrid[ret.RowIndex, ret.ColomnIndex] = 1;

                Ellipse e = new Ellipse();
                e.ToolTip = entity.Name;
                e.Width = 3.5;
                e.Height = 3.5;
                if (entity is SwitchEntity)
                {
                    e.Fill = Brushes.Brown;
                    e.Stroke = Brushes.Brown;
                }
                else if (entity is SubstationEntity)
                {
                    e.Width = 5.5;
                    e.Height = 5.5;
                    e.Fill = Brushes.Blue;
                    e.Stroke = Brushes.Blue;
                }
                else if (entity is NodeEntity)
                {
                    e.Width = 4.5;
                    e.Height = 4.5;
                    e.Fill = Brushes.DarkCyan;
                    e.Stroke = Brushes.DarkCyan;
                }

                double x = (entity.RowIndex * 3.5) - 2;
                double y = (entity.ColomnIndex * 3.5) - 2;
                Canvas.SetTop(e, x);
                Canvas.SetLeft(e, y);
                Canvas.SetZIndex(e, 2);
                ShapeCanvas.Children.Add(e);


            }
        }

        private void ReturnGridEntity(double latitude, double longitude, out int rowIndx, out int colIndx, out double gridLatitude, out double gridLongitude)
            {
                rowIndx = (int)(Math.Round((xmax - latitude) / LatitStep));
                colIndx = (int)(Math.Round((longitude - ymin) / LongStep)); // vraca koordinate grida gde ce se entite smestiti.

                gridLatitude = xmax - rowIndx * LatitStep;
                gridLongitude = ymin + colIndx * LongStep;
            }
        public void GetLatLon()
        {
            double lon = 0;
            double lat = 0;

            foreach (SubstationEntity sse in NetworkModel.Substations)
            {
                ToLatLon(sse.X, sse.Y, out lat, out lon);
                sse.Latitude = lat;
                sse.Longitude = lon;
                allLatitudes.Add(lat);
                allLongitudes.Add(lon);
                dic.Add(new Tuple<string, Entity>("Substation", sse));
            }
            foreach (SwitchEntity swe in NetworkModel.Switches)
            {
                ToLatLon(swe.X, swe.Y, out lat, out lon);
                swe.Latitude = lat;
                swe.Longitude = lon;
                allLatitudes.Add(lat);
                allLongitudes.Add(lon);
                dic.Add(new Tuple<string, Entity>("Switch", swe));
            }
            foreach (NodeEntity ne in NetworkModel.Nodes)
            {
                ToLatLon(ne.X, ne.Y, out lat, out lon);
                ne.Latitude = lat;
                ne.Longitude = lon;
                allLatitudes.Add(lat);
                allLongitudes.Add(lon);
                dic.Add(new Tuple<string, Entity>("Node", ne));
            }

            xmin = allLatitudes.Min(x => x); //moze bez ovoga iz zagrade?
            xmax = allLatitudes.Max(x => x);
            ymin = allLongitudes.Min(y => y);
            ymax = allLongitudes.Max(y => y);
        }

        public static void ToLatLon(double utmX, double utmY, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = true;

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = 34;
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }

        public void Putanje()
        {

            foreach (LineEntity lnEntity in NetworkModel.Lines)
            {
                bool flag = TrazimoPutic(lnEntity.Id, lnEntity.FirstEnd, lnEntity.SecondEnd);
                if (flag == true)
                {
                    CrtamoPutic(Routes[lnEntity.Id]);
                }
            }

            
                    }
        private bool TrazimoPutic(string ID, string firstEnd, string secoundEnd)
        {
            Route route = new Route();

            route.FirstEnd = GetEntityById(firstEnd);
            route.SecondEnd = GetEntityById(secoundEnd);

            if (route.FirstEnd == null || route.SecondEnd == null)
            {
                return false;
            }

            route.Id = ID;

            if (!RouteSearch(route))
            {
                return false;
            }
            else
            {
                Routes[route.Id] = route;
            }
            return true;

        }

        private bool RouteSearch(Route route)
        {
            List<LocationOnGrid> FirstEndCandidates = GetCandidates(route.FirstEnd);
            List<LocationOnGrid> SecondEndCandidates = GetCandidates(route.SecondEnd);

            State solution;

            foreach (LocationOnGrid firstEndCandidate in FirstEndCandidates)
            {
                foreach (LocationOnGrid secondEndCandidate in SecondEndCandidates)
                {
                    if ((solution = BreadthFirstSearch(firstEndCandidate, secondEndCandidate)) != null)
                    {
                        route.RouteOnGrid = solution.Path();
                        route.FirstEnd.Connected = true;
                        route.SecondEnd.Connected = true;

                        return true;
                    }
                }
            }

            return false;
        }

        private State BreadthFirstSearch(LocationOnGrid firstEndCandidate, LocationOnGrid secondEndCandidate)
        {
            List<State> Queue = new List<State>();

            Queue.Add(new State(null, firstEndCandidate));

            while (Queue.Count > 0)
            {
                State Current = Queue[0];
                Queue.Remove(Current);

                if (State.PassedLocation[Current.Node.RowIndex, Current.Node.ColomnIndex] == 1)
                {
                    continue;
                }
                State.PassedLocation[Current.Node.RowIndex, Current.Node.ColomnIndex] = 1;

                if (Current.Node.RowIndex == secondEndCandidate.RowIndex
                    && Current.Node.ColomnIndex == secondEndCandidate.ColomnIndex)
                {
                    for (int i = 0; i <= MainWindow.NumberOfRow; i++)
                    {
                        for (int j = 0; j <= MainWindow.NumberOfColoms; j++)
                        {
                            State.PassedLocation[i, j] = 0;
                        }
                    }
                    return Current;
                }
                else
                {
                    List<State> temp = Current.Children();
                    foreach (State st in temp)
                    {
                        if (MainWindow.MainGrid[st.Node.RowIndex, st.Node.ColomnIndex] == 0)
                        {
                            Queue.Add(st);
                        }
                        if (st.Node.RowIndex == secondEndCandidate.RowIndex && st.Node.ColomnIndex == secondEndCandidate.ColomnIndex)
                        {
                            for (int i = 0; i <= MainWindow.NumberOfRow; i++)
                            {
                                for (int j = 0; j <= MainWindow.NumberOfColoms; j++)
                                {
                                    State.PassedLocation[i, j] = 0;
                                }
                            }
                            return st;
                        }
                    }
                }
            }

            for (int i = 0; i <= MainWindow.NumberOfRow; i++)
            {
                for (int j = 0; j <= MainWindow.NumberOfColoms; j++)
                {
                    State.PassedLocation[i, j] = 0;
                }
            }
            return null;
        }

        private List<LocationOnGrid> GetCandidates(Entity entity)
        {
            List<LocationOnGrid> ret = new List<LocationOnGrid>();

            if (entity.Connected)
            {
                foreach (KeyValuePair<string, Route> kvp in Routes)
                {
                    if (kvp.Value.FirstEnd == entity || kvp.Value.SecondEnd == entity)
                    {
                        if (kvp.Value.RouteOnGrid.Count <= 2)
                        {
                            continue;
                        }
                        ret.AddRange(kvp.Value.RouteOnGrid.GetRange(1, kvp.Value.RouteOnGrid.Count - 2));
                    }
                }
            }
            else
            {
                ret.Add(new LocationOnGrid(entity.RowIndex, entity.ColomnIndex));
            }

            return ret;
        }
        private Entity GetEntityById(string id)
        {
            foreach (SubstationEntity sse in NetworkModel.Substations)
            {
                if (sse.Id.Equals(id))
                {
                    return sse;
                }
            }
            foreach (NodeEntity ne in NetworkModel.Nodes)
            {
                if (ne.Id.Equals(id))
                {
                    return ne;
                }
            }
            foreach (SwitchEntity swe in NetworkModel.Switches)
            {
                if (swe.Id.Equals(id))
                {
                    return swe;
                }
            }
            return null;
        }

        public void CrtamoPutic(Route r)
        {
            int i = 0;
            while (i < r.RouteOnGrid.Count - 1)
            {

                Line line = new Line();
                
                line.X1 = r.RouteOnGrid[i].ColomnIndex * 3.5;
                line.Y1 = r.RouteOnGrid[i].RowIndex * 3.5;
                
                line.X2 = r.RouteOnGrid[i + 1].ColomnIndex * 3.5;
                line.Y2 = r.RouteOnGrid[i + 1].RowIndex * 3.5;

                line.Stroke = Brushes.Red;
                line.StrokeThickness = 1;


                Canvas.SetZIndex(line, 1);
                Canvas.SetTop(line, 0);
                Canvas.SetLeft(line, 0);
                ShapeCanvas.Children.Add(line);


                if ((i == 0 && (r.FirstEnd.RowIndex != r.RouteOnGrid[0].RowIndex || r.FirstEnd.ColomnIndex != r.RouteOnGrid[0].ColomnIndex)) //provera ako je prvi a nije firstend i isto za secoudend
                    || (i == r.RouteOnGrid.Count - 2 &&
                    (r.SecondEnd.RowIndex != r.RouteOnGrid[r.RouteOnGrid.Count - 1].RowIndex
                    || r.SecondEnd.ColomnIndex != r.RouteOnGrid[r.RouteOnGrid.Count - 1].ColomnIndex)))
                {
                    int idx = 0;
                    if (i == 0)
                    {
                        idx = 0;
                    }
                    else
                    {
                        idx = r.RouteOnGrid.Count - 1;
                    }

                    Rectangle rec = new Rectangle();
                    rec.Width = 2.5;
                    rec.Height = 2.5;
                    rec.Fill = rec.Stroke = Brushes.DarkMagenta;

                    double x = r.RouteOnGrid[idx].RowIndex * 3.5;
                    double y = r.RouteOnGrid[idx].ColomnIndex * 3.5;
                    Canvas.SetTop(rec, x);
                    Canvas.SetLeft(rec, y);
                    Canvas.SetZIndex(rec, 2);
                    ShapeCanvas.Children.Add(rec);

                }
                i++;

            }
        }

    }
}
